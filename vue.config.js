module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },

  pwa: {
    name: 'MinesTrix'
  }
}