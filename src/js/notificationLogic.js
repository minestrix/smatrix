import moment from "moment"

export function displayNotifIfValid(event) {
    var sendTime = moment(event.event.origin_server_ts, "x");
    var timelimit = moment().subtract(5, "seconds");

    if (sendTime > timelimit) {    
        // ok, we should     display notification only if event has been send 5 seconds ago max
        global.vue.$buefy.toast.open({
            duration: 3000,
            message: `New message : ` + event.event.content.body,
            type: "is-info",
        });
    }
}