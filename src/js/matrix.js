import olm from "olm";
olm.init();

import matrixsdk from 'matrix-js-sdk'
import axios from 'axios'

const roomSuffix = "@";


export function getRoomNameFromUserUsername(userid) {
    return userid + " timeline";
}

// small helpers
export function getUsernameFromRoomName(roomName) {
    return roomName.split(" ")[0].replace(" ", "");
}


export function isRoomUserFeed(roomName) {
    if (roomName.startsWith(roomSuffix)) {
        return true;
    }
    return true;
}


export function getUser(userid) {
    return global.matrixclient.getUser(userid);
}

/**
 * Return a Mines'Trix client
 */
export function getMinesTrixClient() {
    var data = {
        accountcreated: false,
        userRoomID: null,
        roomsid: [],
        rooms: [],
        roomInvites: [],
        friendsInvites: [],
        friends: [],
        userTimeline: [],
        selectUserDetails: { room: null, userid: localStorage.userid, roomid: null },
        timeline: [],
        /**
        * Load the valid rooms and friends invites
        */
        loadValidRooms: async function () {
            var rooms = await client.getRooms();

            this.rooms = [];
            this.friendsInvites = [];

            for (let index = 0; index < rooms.length; index++) {
                var room = rooms[index];
                if (isValidSMatrixRoom(room)) {
                    console.log(room.name);

                    var userid = getUsernameFromRoomName(room.name);

                    if (room._selfMembership == "join") {
                        this.rooms.push(room);
                        if (userid == localStorage.userid) {
                            this.accountcreated = true;
                            this.userRoomID = room.roomId;
                        }
                    }
                    else if (room._selfMembership == "invite") {

                        var username = {
                            userid: getUsernameFromRoomName(room.name),
                            membership: "invite",
                            name: room.currentState.members[userid].name,
                            roomname: room.name
                        };
                        this.roomInvites.push(room);
                        this.friendsInvites.push(username);
                    }
                    else {
                        console.log("Room not categorized : ");
                        console.log(room);
                    }
                }
            }
        },

        loadFriends: async function () {
            if (this.userRoomID !== null) {

                var members = await global.matrixclient.members(this.userRoomID, "", "", "");
                members = members["chunk"];

                this.friends = []; // clear the list
                for (var pos in members) {

                    var contact = {
                        userid: members[pos]["state_key"], // well
                        membership: members[pos]["content"]["membership"],
                        name: members[pos]["content"]["displayname"],
                    };

                    if (contact.membership == "invite") contact.membership = "request";

                    if (members[pos].content.avatar_url != null) {
                        contact.avatar_url = members[pos].content.avatar_url;
                    }

                    this.friends.push(contact);
                }
                return true;
            }
            else {
                console.log("SMATRIX : userRoomID is not set");
                return false;
            }
        },
        loadUserTimeline: async function (userid) {
            var roomname = null;

            // get the room id associated with the current user id
            if (userid) {
                roomname = getRoomNameFromUserUsername(userid);
            }
            // failback
            if (roomname == null) { roomname = getRoomNameFromUserUsername(localStorage.userid) }

            if (this.rooms) {
                var events = [];

                this.rooms.forEach(room => {
                    if (room !== null && room.name == roomname) {

                        // load data
                        this.selectUserDetails.room = room;
                        this.selectUserDetails.userid = userid;
                        this.selectUserDetails.roomid = room.userId;

                        room.timeline.forEach(event => {
                            events.push(event);
                        })
                    }
                });

                // sort timeline
                events = await sortTimeline(events);
                this.userTimeline = events;
            }
        },
        /**
       * Load timeline, valid rooms should have been loaded first
       */
        loadTimeline: async function () {
            if (this.rooms) {
                var events = [];
                this.rooms.forEach(room => {
                    if (room !== null) {
                        room.timeline.forEach(event => {
                            events.push(event);
                        })
                    }
                    else {
                        console.warn("Could not load room...");
                    }
                });

                // sort timeline
                events = await sortTimeline(events);
                this.timeline = events;
            }
        },


        loadAndSort: async function (room) {
            if (room) {
                await global.matrixclient.scrollback(room);
                this.timeline = await sortTimeline(this.timeline);
            }
            else {
                console.error("room is null");
            }
        },

        sendPost: async function (post, userRoomID) {

            if (userRoomID === undefined) {
                userRoomID === global.userRoomID;
            }

            if (userRoomID !== null) {
                const content = {
                    "body": post,
                    "msgtype": "m.text"
                };
                await global.matrixclient.sendEvent(userRoomID, "m.room.message", content, "");
                return true;
            }
            else {
                console.log("SMATRIX : can't post message, userRoomID is not set");
                return false;
            }
        },
        getUsers: function () {
            return global.matrixclient.getUsers();
        },
        addFriend: async function (userid) {
            if (this.userRoomID !== null) {
                await global.matrixclient.invite(this.userRoomID, userid);
                return true;
            }
            else {
                console.log("SMATRIX : userRoomID is not set");
                return false;
            }
        },

        /**
       * Mines'Trix client initialisation, to know if we need to create an account
       */
        init: async function () {
            await this.loadValidRooms();
        },
        sync: async function (userid) {
            await this.loadValidRooms();
            await this.loadFriends();
            await this.loadTimeline();
            await this.loadUserTimeline(userid);
        },
        // friends invites
        acceptFriendRequest: async function (userid) {
            var roomName = getRoomNameFromUserUsername(userid);
            var roomid = this.roomNameToRoomId(roomName);
            await client.joinRoom(roomid);
        },
        /**
        * Loop through friend requests and accept those already accepted by the sender.
        */
        acceptAlreadyAddedFriends: async function () {

            var shouldRlead = false;
            this.friends.forEach(friend => {
                this.friendsInvites.forEach(fInvite => {
                    if (friend.userid == fInvite.userid) {
                        console.log("We can accept " + friend.userid + " invitation");
                        this.acceptFriendRequest(friend.userid);
                        shouldRlead = true;
                    }
                });
            });
            return shouldRlead;
        },

        // conversions
        roomIdToRoomName: function (roomid) {
            var roomname;

            // search in joined rooms
            this.rooms.forEach(room => {
                if (room.roomId == roomid) {
                    roomname = room.name;
                }
            });

            // search in room invites
            this.roomInvites.forEach(room => {
                if (room.roomId == roomid) {
                    roomname = room.name;
                }
            });

            return roomname;
        },
        roomNameToRoomId: function (roomName) {
            var roomid;

            // search in joined rooms
            this.rooms.forEach(room => {
                if (room.name == roomName) {
                    roomid = room.roomId;
                }
            });

            // search in room invites
            this.roomInvites.forEach(room => {
                if (room.name == roomName) {
                    roomid = room.roomId;
                }
            });

            return roomid;
        },
        roomNameToUser: function (roomName) {
            if (roomName) {
                if (isRoomUserFeed(roomName)) {
                    var userid = roomName.split(" ")[0].replace(" ", "");
                    return global.matrixclient.getUser(userid);
                }
            }
            else return null;
        },
        /**
         * Return the roomid associated with a userid
         * @param {*} userid 
         */
        usernameToRoomId: function (userid) {
            return this.roomNameToRoomId(userid + " timeline");
        },




        // diverse
        isRoomEncrypted: function (room) {
            return global.matrixclient.isRoomEncrypted(room.roomId);
        }






    }

    return data;
}

export async function loadMore(room) {
    await global.matrixclient.scrollback(room);
    return sortTimeline(room.timeline);
}

export function isValidSMatrixEvent(event) {
    var roomid = event.room_id;
    var room = global.matrixclient.getRoom(roomid);
    return isValidSMatrixRoom(room);
}

// return true if the given room is a valid smatrix room
// in particular check if the specific room is created by the specific user
export function isValidSMatrixRoom(room) {
    if (room.name.startsWith(roomSuffix)) {
        // setting up variables
        var username = getUsernameFromRoomName(room.name);

        if(room.name != getRoomNameFromUserUsername(username)){
            console.log("Invalid room name : |"  + room.name + "| : |" + username + "|");
            return false;
        }
        var members = room.currentState.members;
        console.log("is valid ? " + room.name + " |" + username + "|");
        console.log(members[username]);

        if (members[username] == undefined) return false;

        // check power levels
        if (members[username].powerLevel == 100 && members[username].membership == "join") {
            return true;
        }
        else if (room._selfMembership == "invite") { // we need to check if the  conversation is nothijacked
            return true;
        }
        /*  else {
              // could be an usurpation
              console.log(username + " membership : " + members[username].membership + " power level : " + members[username].powerLevel);
          }*/
    }
    return false;
}



export async function connect(hs, mxid, password) {
    if (!hs) {
        // Prettier wanted me to put each function call on a new line, which I
        // thought was stupid and unreadable
        // eslint-disable-next-line
        const base_url = 'https://' + mxid.split(':').slice(1).join(':')
        const { data } = await axios.get(base_url + '/.well-known/matrix/client')

        if (!data || !data['m.homeserver'] || !data['m.homeserver'].base_url) {
            throw new Error('Well-known lookup did not contain homeserver URL')
        }
        hs = data['m.homeserver'].base_url
    }


    const client = matrixsdk.createClient({
        baseUrl: hs,
        deviceId: "deviceid",
        sessionStore: new matrixsdk.WebStorageSessionStore(window.localStorage),
        cryptoStore: new matrixsdk.MemoryCryptoStore()
    });


    const response = await client.loginWithPassword(mxid, password);
    if (!response || !response.access_token || !response.user_id) {
        throw new Error('Server returned an invalid login response')
    }
    global.mclient = client;

    // store data
    localStorage.token = response.access_token;
    localStorage.mxid = response.user_id;

    return {
        token: response.access_token,
        mxid: response.user_id,
        client: client,
        hs,
    }
}

var client;

export function launchConnection() {
    client = matrixsdk.createClient({
        baseUrl: localStorage.hs,
        userId: localStorage.userid,
        accessToken: localStorage.token,
        deviceId: "minestrixcilent",
        sessionStore: new matrixsdk.WebStorageSessionStore(window.localStorage),
        cryptoStore: new matrixsdk.MemoryCryptoStore()
    });

    global.matrixclient = client;

    client.initCrypto();
    client.startClient();

    return client;
}


// return true if the logged user has been used
export function isClientCredentialStored() {
    if (localStorage.getItem("userid") !== null && localStorage.getItem("token") !== null && localStorage.getItem("hs") !== null) {
        return true;
    }
    return false;
}

export function storeCredentials(hs, userid, token) {
    localStorage.hs = hs;
    localStorage.token = token;
    localStorage.userid = userid;
}



// matrix functions :
export async function getJoinedRooms() {
    var a = await client.getJoinedRooms();
    return a.joined_rooms;
}

export async function sendPost(post) {
    if (global.userRoomID !== null) {
        const content = {
            "body": post,
            "msgtype": "m.text"
        };
        await global.matrixclient.sendEvent(global.userRoomID, "m.room.message", content, "");
        return true;
    }
    else {
        console.log("SMATRIX : can't post message, userRoomID is not set");
        return false;
    }
}

export async function sendFriendInvite(username) {
    if (global.userRoomID !== null) {
        await global.matrixclient.invite(global.userRoomID, username);
        return true;
    }
    else {
        console.log("SMATRIX : userRoomID is not set");
        return false;
    }
}

export async function sortTimeline(timeline) {
    for (let index = 0; index < timeline.length; index++) {
        for (let bublle = index; bublle > 0; bublle--) {
            if (timeline[bublle].event.origin_server_ts > timeline[bublle - 1].event.origin_server_ts) {
                var temp = timeline[bublle];
                timeline[bublle] = timeline[bublle - 1];
                timeline[bublle - 1] = temp;
            }
        }
    }
    return timeline;
}

export async function createSMatrixAccount() {
    var roomname = "@" + localStorage.userid;
    console.log("room name : " + roomname);

    var options = {
        visibility: "private",
        invite: [],
        name: roomname,
        topic: "Smatrix room",
        //room_alias_name: roomname,
    };
    var room = await global.matrixclient.createRoom(options);
    global.userRoomID = room.room_id;
}
