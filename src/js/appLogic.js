
import {
    isClientCredentialStored,
    launchConnection,
    isValidSMatrixEvent,
    getMinesTrixClient
} from "../js/matrix";

import {displayNotifIfValid} from "./notificationLogic"

async function smatrixInitialasiation() {


    var mtclient = getMinesTrixClient();
    /**
   * Initialize the Mines'Trix client to know if the client has already been created (i.e the room exists) 
   * */
    await mtclient.init();


    // make it globaly accesible
    global.vue.$store.state.mtclient = mtclient;

    console.info("Mines'Trix client ready");

    if (mtclient.accountcreated == false) {
        console.log("Need to create a room... rerouting...");
        global.vue.$router.push("createAccount");
    }
    else {
        try {
            await mtclient.sync();
            await mtclient.acceptAlreadyAddedFriends();
        } catch (ex) {
            console.log(ex);
        }
    }

    // UI
    global.vue.$store.state.smatrixReady = true;
    global.vue.$store.state.connected = true;
}

export async function applicationLogic() {
    console.log("App logic...");

    try {
        if (isClientCredentialStored()) {
            // ok, now, we can launch matrix
            try {
                launchConnection(); // connect to matrix
                console.log("done");
                if (global.matrixclient.isLoggedIn()) {
                    global.vue.$buefy.toast.open({
                        duration: 2000,
                        message: `Connected`,
                        type: "is-success",
                    });
                }


            } catch (ex) {
                console.log(ex);
                console.log("Is logged in : " + global.matrixclient.isLoggedIn());
                global.vue.$buefy.toast.open({
                    duration: 3000,
                    message: `could not connect to the server`,
                    type: "is-danger",
                });
            }


            global.matrixclient.on("sync", async function (state) {
                try {
                    switch (state) {
                        case "ERROR":
                            // update UI to say "Connection Lost"
                            global.vue.$store.state.connected = false;
                            global.vue.$buefy.toast.open({
                                duration: 5000,
                                message: `Connection lost`,
                                type: "is-danger",
                            });
                            console.log("Connection lost...");

                            break;
                        case "SYNCING":
                            // update UI to remove any "Connection Lost" message
                            global.vue.$store.state.connected = true;

                            break;
                        case "PREPARED":
                            global.prepared = true;


                            // we can initialise the matrix client...
                            await smatrixInitialasiation();

                            // the client instance is ready to be queried.
                            console.log("prepared...");



                            break;
                    }
                }
                catch (ex) {
                    console.log("Error on sync event : ");
                    console.log(ex);
                }
            });

            // on new message
            global.matrixclient.on("Room.timeline", function (event) {
                if (event.getType() !== "m.room.message") {
                    return; // only use messages
                }

                if (global.prepared) {
                    if (isValidSMatrixEvent(event.event)) {
                        // add event to the timeline
                        global.vue.$store.state.mtclient.timeline.unshift(event);
                        displayNotifIfValid(event);
                       
                    }
                }
            });


            global.matrixclient.onDecryptedMessage = message =>  {
                
                console.log('Got encrypted message: ', message);
            }
        
            global.matrixclient.on('Event.decrypted', (event) => {
                if (event.getType() === 'm.room.message'){
                    global.matrixclient.onDecryptedMessage(event.getContent().body);
                } else {
                    console.log('decrypted an event of type', event.getType());
                    console.log(event);
                }
            });
          

            /*
                        global.vue.$buefy.toast.open({
                            duration: 3000,
                            message: `Message from app logic`,
                            type: "is-info",
                        });
            */
        }

    } catch (e) {
        // Deal with the fact the chain failed
        console.log("error : ")
        console.log(e);
    }



}

