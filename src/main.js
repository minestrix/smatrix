document.title = "Mines'Trix";

// UI
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import "@/js/registerServiceWorker";

// create store
const store = new Vuex.Store({
    state: {
        joinedRoomsId: [],
        smatrixRooms: [], // list all the already joined rooms
        smatrixRoomsInvites: [], // list all the smatrix room with an invitation pending
        friends: [],
        friendsInvites: [],
        accountTimeline: [],
        connected: false,
        smatrixReady: false,
        debug: false,
        mtclient: null // minestrix client client
    }
})


Vue.config.productionTip = false

// vue router
import VueRouter from 'vue-router'
Vue.use(VueRouter)



// font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(fas);
library.add(fab);

Vue.component('vue-fontawesome', FontAwesomeIcon);

// vue.js
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy, {
    defaultIconComponent: 'vue-fontawesome',
    defaultIconPack: 'fas',
});

// pages
import loginpage from './Pages/loginpage'
import homepage from './Pages/homepage'

// views
import AccountFeed from "./components/Views/AccountFeed";
import AccountFeedOld from "./components/Views/AccountFeed-old";
import Feed from "./components/Views/Feed";
import PostEditor from "./components/Views/PostEditor";
import Contact from "./components/Views/ContactView";
import AccountCreation from "./components/Views/AccountCreation";
import Debug from "./components/Views/Debug";
import Settings from "./components/Views/Settings";


const routes = [
    { path: '/login', component: loginpage },
    {
        path: '/', component: homepage, redirect:'/feed', children: [
            { path: '/feed', component: Feed },
            {
                path: '/user-feed/:userid',
                component: AccountFeed,
                props: true,
            },
            { path: '/user-feed', component: AccountFeed }, // here is the current user
            { path: '/user-old', component: AccountFeedOld },
            { path: '/posted', component: PostEditor },
            { path: '/friends', component: Contact },
            { path: '/createAccount', component: AccountCreation },
            { path: '/debug', component: Debug },
            { path: '/settings', component: Settings }]
    }
]


const router = new VueRouter({
    routes // short for `routes: routes`
})


// matrix definition : see js/matrix.js
import { isClientCredentialStored } from "./js/matrix";


router.beforeEach((to, from, next) => {
    if (!isClientCredentialStored()) {
        if (to.path != '/login')
            next('/login');
        else next();
    }
    else {
        if (to.path != '/login')
            next();
        else next(false);
    }
})



import App from './App'

global.vue = new Vue({
    store: store,
    render: h => h(App),
    router
}).$mount('#app');


import { applicationLogic } from "./js/appLogic";

applicationLogic();